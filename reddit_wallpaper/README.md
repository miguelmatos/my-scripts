# Reddit Wallpaper

## Description

Download images from the subreddit r/wallpaper and set one of them as the wallpaper.

## How it works

The script will first download images from r/wallpaper through the use of `praw`, then it will set randomly one of the first images in *Hot*.

## How to run

First install the dependecies with pip

```
pip install -r requirements.txt
```

Then just run the script

```
python3 main.py
```
