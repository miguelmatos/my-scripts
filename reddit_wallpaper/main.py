#!/bin/env python3

import praw
import requests
import random
import os


CLIENT_ID = "xxx"
CLIENT_SECRET = "xxx"
PICTURES_DIRECTORY = "/home/user/Pictures/"


def authenticate():
    reddit = praw.Reddit(client_id=CLIENT_ID,
                         client_secret=CLIENT_SECRET,
                         user_agent="testscript")
    return reddit


def pictures(reddit, limit=4):
    subr = reddit.subreddit('wallpaper')
    return subr.hot(limit=limit)


def set_wallpaper(limit=4):
    r = random.randint(0, limit - 1)
    os.system(
        f"/usr/bin/gsettings set org.gnome.desktop.background picture-uri {PICTURES_DIRECTORY}/f{r}.png")


r = authenticate()
pics = pictures(r)

for i, p in enumerate(pics):
    with open(f"{PICTURES_DIRECTORY}/f{i}.png", "wb") as f:
        req = requests.get(p.url)
        f.write(req.content)
        f.close()

set_wallpaper()
