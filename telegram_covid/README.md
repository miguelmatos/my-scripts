# SMS Covid

Bot que envia dados para um channel do telegram sempre que os dados de Covid em Portugal saírem.

Pode-se encontrar o channel do telegram [aqui](https://t.me/dailycovidpt).

## Formato da mensagem

A mensagem tem o seguinte formato:

```
20/09/2020
Novos Casos: 68577 | +552
Óbitos: 1912 | +13
Recuperados: 45596 | +192
```

## Correr o programa

Para correr o programa deve-se instalar as dependências com

```
$ pip install -r requirements.txt
```

Depois corre-se 

```
$ python3 clock.py
```

## Funcionamento

O programa funciona da seguinte maneira:

- Verifica se a mensagem que está pinned no telegram foi enviada no dia ou se foi noutro dia, se for noutro dia, então o programa espera 1 hora antes de voltar a fazer esta verificação
- Caso contrário, então o programa vai ver todos os minutos se o pdf do dia já saiu no site da [dgs](https://covid19.min-saude.pt/relatorio-de-situacao/)
- Quando o relatório já tiver saído então é feito o download do mesmo e extraído os valores do dia
- O bot envia uma mensagem para o chat com a atualização dos valores e faz pin dessa mensagem
- Por último, os pdf's são todos apagados.
