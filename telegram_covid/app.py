#!/bin/env python3
import glob
import os
import time
from datetime import datetime

import pdfplumber
import requests
from lxml import html

BOT_TOKEN = 'xxx'
BOT_CHAT_ID = '@dailycovidpt'


def download_pdf(day):
    url = 'https://covid19.min-saude.pt/relatorio-de-situacao/'
    print("Downloading pdf...")

    while True:
        page = requests.get(url)
        tree = html.fromstring(page.content)

        today_link = tree.xpath(f"//a[contains(text(), '{day}')]")

        if not today_link:
            print(datetime.now(), "Nada de novo")
            time.sleep(60)
            continue

        r = requests.get(today_link[-1].attrib['href'], stream=True)

        with open(f"{day.replace('/', '_')}.pdf", "wb") as fd:
            for chunk in r.iter_content(2048):
                fd.write(chunk)

            print("Getting pdf")

            pdf = f"{day.replace('/', '_')}.pdf"
            return pdf


def open_pdf(pdf):
    p = pdfplumber.open(pdf)
    page = p.pages[0]

    # Crops 30% of the page
    left_page = page.crop((0, 0, 0.3 * float(page.width), page.height))

    # Split string by the line breaks
    s = left_page.extract_text().split("\n")
    # Reverse the list
    s.reverse()

    return s


def get_data(words_lst):
    data = dict()
    for i, s in enumerate(words_lst):
        if s == "CONFIRMADOS":
            new_data = [int(x.replace(" ", "")) for x in words_lst[i + 1].split("+")]
            data['confirmados'] = {'total': new_data[0], 'novos': new_data[1]}
        elif s == "ÓBITOS":
            new_data = [int(x.replace(" ", "")) for x in words_lst[i + 1].split("+")]
            data['óbitos'] = {'total': new_data[0], 'novos': new_data[1]}
        elif s == "RECUPERADOS":
            new_data = [int(x.replace(" ", "")) for x in words_lst[i + 1].split("+")]
            data['recuperados'] = {'total': new_data[0], 'novos': new_data[1]}

    return data


def send_data(data, day):
    # %26 = +
    msg = f"<b>{day}</b>\n" \
          f"Novos Casos: {data['confirmados']['total']} | %2B{data['confirmados']['novos']}\n" \
          f"Óbitos: {data['óbitos']['total']} | %2B{data['óbitos']['novos']}\n" \
          f"Recuperados: {data['recuperados']['total']} | %2B{data['recuperados']['novos']}"

    chat = f'https://api.telegram.org/bot{BOT_TOKEN}/unpinChatMessage?chat_id={BOT_CHAT_ID}'
    print(requests.get(chat).json())

    send_text = 'https://api.telegram.org/bot' + BOT_TOKEN + '/sendMessage?chat_id=' + BOT_CHAT_ID + \
                '&parse_mode=HTML&text=' + msg

    response = requests.get(send_text)
    print(response.json())

    message_id = response.json()['result']['message_id']
    pin_message = f'https://api.telegram.org/bot{BOT_TOKEN}/pinChatMessage?' \
                  f'chat_id={BOT_CHAT_ID}&message_id={message_id}&' \
                  f'disable_notification=True'
    print(requests.get(pin_message).json())

    return response.json()


def delete_pdfs():
    for i in glob.glob("*.pdf"):
        os.remove(i)


def get_pinned_message_date():
    chat = f'https://api.telegram.org/bot{BOT_TOKEN}/getChat?chat_id={BOT_CHAT_ID}'
    message = requests.get(chat).json()['result']['pinned_message']

    return message['text'].split('\n')[0]


if __name__ == '__main__':
    while True:
        today = datetime.today().strftime("%d/%m/%Y")
        if today == get_pinned_message_date():
            # Checking if message was already sent
            print("Message already sent")
            time.sleep(60 * 60)
        else:
            rel = download_pdf(datetime.today().strftime("%d/%m/%Y"))
            words_list = open_pdf(rel)
            send_data(get_data(words_list), datetime.today().strftime("%d/%m/%Y"))
            delete_pdfs()
